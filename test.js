const assert = require('assert')
const resolvers = require('.')

assert.ok(Array.isArray(resolvers.dns))
assert.ok(resolvers.dns.length > 0)

assert.ok(Array.isArray(resolvers.doh))
assert.ok(resolvers.doh.length > 0)

assert.ok(resolvers.aliased.dns instanceof Map)
assert.ok(resolvers.aliased.doh instanceof Map)

assert.strictEqual(
  resolvers.aliased.dns.size,
  resolvers.dns.length
)
assert.strictEqual(
  resolvers.aliased.doh.size,
  resolvers.doh.length
)

for (const resolver of resolvers.dns) {
  assert.strictEqual(Object.keys(resolver).length, 4)
  assert.ok(resolver.alias)
  assert.ok(resolver.name)
  assert.ok(resolver.dns4)
  assert.ok(resolver.dns6)
}

for (const resolver of resolvers.doh) {
  assert.strictEqual(Object.keys(resolver).length, 3)
  assert.ok(resolver.alias)
  assert.ok(resolver.name)
  assert.ok(resolver.doh)
}

for (const [ , resolver ] of resolvers.aliased.dns) {
  assert.strictEqual(Object.keys(resolver).length, 3)
  assert.ok(!resolver.alias)
}

for (const [ , resolver ] of resolvers.aliased.doh) {
  assert.strictEqual(Object.keys(resolver).length, 2)
  assert.ok(!resolver.alias)
}
